const mongoose = require('mongoose');

const passengerSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        minlength: 5,
        maxlength: 35,
    },
    destination : {
        type : String, 
        required : true,
        minlength: 5,
        maxlength: 35,
    },
    mobileNo : {
        type : String,
        required : true,
        maxlength: 10
    },
    age : {
        type : Number, 
        required : true,
        min: 18
    },
});

module.exports = {
    Passenger: mongoose.model('passenger', passengerSchema),
}